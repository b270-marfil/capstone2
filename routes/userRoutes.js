const express = require("express");
const router = express.Router();
const auth = require("../auth");
const userController = require("../controllers/userController")

// Route for registration (User registration)
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromRegister => res.send(resultFromRegister))
});


// Route for logging in (User authentication)
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromLogin => res.send(resultFromLogin))
});

// Route to set user as admin
router.put("/:userId/setAsAdmin", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)
	if(token.isAdmin === true){
		userController.admin(req.params).then(resultFromUpdatedAdmin => res.send(resultFromUpdatedAdmin))
	}else{
		res.send({auth: "Not an admin"})
	}
});

// Route for users ordering products (create order)
router.post("/checkout", (req, res) => {
	const data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId,
		quantity: req.body.quantity,
		price: req.body.price,
		shippingFee: req.body.shippingFee,
		totalAmount: req.body.totalAmount
	}
	userController.order(data).then(resultFromOrder => res.send(resultFromOrder))	
})

// Route to retrieve all orders
router.get("/orders", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)
	if(token.isAdmin === true){
	userController.retrieveOrders(req.body).then(resultFromRetrieveOrders => res.send(resultFromRetrieveOrders))
	console.log(req.body)
	}else{
		res.send({auth: "Not an Admin"})
	}	
})

// Route to Retrieve User Details
router.get("/:userId/userDetails", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	userController.getProfile(userData.id).then(resultFromGetProfile => res.send(resultFromGetProfile))
})




module.exports = router;
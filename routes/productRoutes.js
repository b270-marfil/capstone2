// The dependencies
const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController')
const auth = require("../auth")

// Route to create a new product (Create Product (Admin only))
router.post("/", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)
	if(token.isAdmin === true){
		productController.addProduct(req.body).then(resultFromAddProduct => res.send(resultFromAddProduct))
	}else{
		res.send({auth: "Not an admin"})
	}
})

// Route to get all active product (Retrieve all active products)
router.get("/", (req, res) => {
	productController.getAll(req.body).then(resultFromGet => res.send(resultFromGet))
}) 

// Route to get a specific product (Retrieve single product)
router.get("/:productId", (req, res) => {
	productController.getProduct(req.params).then(resultFromGetSpecific => res.send(resultFromGetSpecific))
})

// Route to update a product (Update Product information (Admin only))
router.put("/:productId", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)
	if(token.isAdmin === true){
		productController.updateProduct(req.params, req.body).then(resultFromUpdate => res.send(resultFromUpdate))
	}else{
		res.send({auth:"Not an admin"})
	}
})

// route to archive a product (Archive Product (Admin only))
router.patch("/:productId", auth.verify, (req, res) => {
	// console.log(req.params)
	let token = auth.decode(req.headers.authorization)
	if(token.isAdmin === true){
		productController.archiveProduct(req.params, req.body).then(resultFromArchive => res.send(resultFromArchive))
	}else{
		res.send({auth: "Not an Admin"})
	}
})


module.exports = router;

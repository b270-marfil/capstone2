const User = require("../models/user");
const Product = require("../models/product");
const auth = require("../auth");
const bcrypt = require("bcrypt");

// Registration
module.exports.registerUser = (body) => {
	let newUser = new User({
		firstName: body.firstName,
		lastName: body.lastName,
		mobileNumber: body.mobileNumber,
		email: body.email,
		password: bcrypt.hashSync(body.password, 10)
	})

	return newUser.save().then((user, error) => {
		if(error){
			return ('There is an error.');
		}else{
			return ('Success!');
		}
	})
}

// Authenticate or Log In
module.exports.loginUser = (body) => {
	return User.findOne({email: body.email}).then(result => {
		if(result === null){
			return ('User does not exist.');
			// User doesn't exist
		}else{
			const isPasswordCorrect = bcrypt.compareSync(body.password, result.password)

			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result.toObject())}
			}else{
				return ('Cannot log in. User already exist.');
				// can't log in
			}
		}
	})
}

// Set user as admin
module.exports.admin = (params) => {
	let updatedAdmin = {
		isAdmin: true
	}
	return User.findByIdAndUpdate(params.userId, updatedAdmin).then((user, error) => {
		if(error){
			return false
		}else{
			return ('User is now an admin.')
		}
	})
}

// Order of products (Create Order)
module.exports.order = async (data) => {
	
	let userSaveStatus = await User.findById(data.userId).then(user => {
		user.productOrders.push({
			productId: data.productId,
			quantity: data.quantity,
			price: data.price,
			shippingFee: data.shippingFee,
			totalAmount: data.totalAmount
		})
		return user.save().then((user, err) => {
			if(err){
				return ('Product not saved in the users order list.');
				// product not saved in user's productOrders
			}else{
				return ('Product successfully saved in the users list.');
				// product successfully saved in user's productOrders
			}
		})
	})

	let productSaveStatus = await Product.findById(data.productId).then(product => {
		product.orderedByUser.push({userId: data.userId})
		return product.save().then((product, err) => {
			if(err){
				return ('User not saved in the product list.');
				// user not saved in user's orderedByUser
			}else{
				return ('User successfully saved in the product list.');
				// user successfully saved in orderedByUser
			}
		})
	})

	if(userSaveStatus && productSaveStatus){
		return ('Successfully ordered!');
	}else{
		return false;
	}
}

// Getting user's cart orders
module.exports.getProfile = (userId) => {
	return User.findById(userId).then(result => {
		result.password = undefined;
		return result;
	})
}

module.exports.retrieveOrders = () => {
	return User.find({isAdmin: false}).then(users => {
		let allOrders = [];
		users.forEach(user => {
			allOrders.push({
				email: user.email,
				userId: user._id,
				orders: user.productOrders
			});
		})
		return allOrders;
	})
}

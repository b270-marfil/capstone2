const Product = require('../models/product.js')

module.exports.addProduct = (body) => {

	let newProduct = new Product({
		name: body.name,
		description: body.description,
		color: body.color,
		price: body.price,
		quantity: body.quantity
	})

	return newProduct.save().then((product, error) => {
		if(error){
			return ('Save unsuccessful. Please try again'); 
		// save unsuccessful
		}else{
			return ('Save successful!');
		// save successful
		}
	})
}

module.exports.getAll = () => {
		return Product.find({isActive: true}).then(result => {
			return result;
		})		
}

module.exports.getProduct = (params) => {
	return Product.findById(params.productId).then(result => {
		return result;
	})
}

module.exports.updateProduct = (params, body) => {
	let updatedProduct = {
		name: body.name,
		description: body.description,
		color: body.color,
		price: body.price,
		quantity: body.quantity
	}
	return Product.findByIdAndUpdate(params.productId, updatedProduct).then((course, error) => {
		if(error){
			return ('There is an error. Please try again')
		}else{
			return ('Update successful!')
		}
	})
}

module.exports.archiveProduct = (params, body) => {
	console.log(params.productId)

	let updatedProduct
		
		if(body.isActive === true){
			updatedProduct = {
				isActive: false
			}
		}else{
			updatedProduct = {
				isActive: true
			}
		}
	console.log(updatedProduct)
	return Product.findByIdAndUpdate(params.productId, updatedProduct).then((product, error) => {
		if(error){
			return ('There is an error. Please try again')
		}else{
			return ('Archive successful!')
		}
	})
}


const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	
	name: {
		type: String,
		required: [true, "Product name is required"]
	},
	description: {
		type: String,
		required: [true, "Product description is required"]
	},
	color: {
		type: String
	},
	price: {
		type: Number
	},
	quantity: {
		type: Number
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	shipping: {
		type: String,
		enum: ['JRSExpress', 'LBC', '2go']
	},
	paymentMethod: {
		type: Number,
		enum: ['Cash', 'Credit Card', 'Bank transfer', 'GCash']
	},
	orderedByUser: [
		{
			userId: {
				type: String
			},
			orderedOn: {
				type: Date,
				default: new Date()
			},
			quantity: {
				type: Number
			},
			price: {
				type: Number
			},
			totalAmount: {
				type: Number
			}
		}
	]
})

module.exports = mongoose.model("Product", productSchema);


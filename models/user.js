const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({

	firstName : {
		type : String,
		required: [true, "Please enter First Name."]
	},
	lastName : {
		type: String,
		required: [true,"Please enter Last Name."]
	},
	email: {
		type: String,
		required: [true, "Please enter Email Address."]
	},
	password: {
		type: String,
		required: [true, "Password is required."]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	mobileNumber: {
		type: String,
		required: [true, "Please enter mobile number."]
	},
	address: [
		{
			shippingAddress: {
				province : {
					type: String,
					required: [true, "Please enter province."]
				},
				city: {
					type: String,
					required: [true, "Please enter city."]
				},
				baranggay: {
					type: String,
					required: [true, "Please enter baranggay."]
				},
				street: {
					type: String,
					required: [true, "Please enter street."]
				},
				lotNumber: {
					type: String,
					required: [true, "Please enter lot number."]
				}
			}
		}
	],
	productOrders: [
		{
			productId: {
				type: String,
				required: [true, "Product Order is required"]
			},
			orderedOn: {
				type: Date,
				default: new Date()
			},
			quantity: {
				type: Number
			},
			price: {
				type: Number
			},
			shippingFee: {
				type: Number
			},
			totalAmount: {
				type: Number
			}	
		}
	]
})

module.exports = mongoose.model("User", userSchema);

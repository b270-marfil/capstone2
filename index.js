// The dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// Server SetUp
const app = express();
const port = 4000;


//The Import routes
const productRoutes = require('./routes/productRoutes');
const userRoutes = require('./routes/userRoutes');

// Connection to MongoDB Atlas
mongoose.connect("mongodb+srv://admin:admin123@zuitt.jxv3idp.mongodb.net/capstone2-ecommerce?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
})


// The link routes / Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use("/users", userRoutes);
app.use("/products", productRoutes);



// Confirmation that it is connected to Atlas
mongoose.connection.once('open', () => {
	console.log("Now connected to MongoDB Atlas.")
})

// Shows that API actually runs
app.listen(process.env.PORT || port, () => {
	console.log(`API is now online on port ${
		process.env.PORT || port
	}`)
})
